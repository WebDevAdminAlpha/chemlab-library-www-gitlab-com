# frozen_string_literal: true

module GitlabHandbook
  module Page
    class FreeTrial < Chemlab::Page
      path '/free-trial'

      link :start_your_gitlab_free_trial_saas, text: 'Start your GitLab.com free trial'
      link :start_your_gitlab_free_trial_self_managed, text: 'Start your Self-Managed GitLab free trial'
    end
  end
end
