# frozen_string_literal: true

module GitlabHandbook
  module Page
    class About < Chemlab::Page
      path '/'

      link :get_free_trial, text: 'Get free trial', href: '/free-trial/'
    end
  end
end
